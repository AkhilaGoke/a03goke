QUnit.test('Testing arithematic function with three sets of inputs', function (assert) {
    assert.throws(function () { addition(); }, new Error("only numbers are allowed"), 'Passing in array correctly raises an Error');
    assert.strictEqual(addition.value(3,4,1), 8, 'All positive numbers');
    assert.strictEqual(subtraction.value(3,-7,1), 9, 'Positive and negative numbers');
    assert.strictEqual(division.value(20,4,5), 1, 'All are negative numbers');
});
